/*
 * Copyright 2009-2017 Alibaba Cloud All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <alibabacloud/dts/model/DescribeDataCheckReportUrlRequest.h>

using AlibabaCloud::Dts::Model::DescribeDataCheckReportUrlRequest;

DescribeDataCheckReportUrlRequest::DescribeDataCheckReportUrlRequest()
    : RpcServiceRequest("dts", "2020-01-01", "DescribeDataCheckReportUrl") {
  setMethod(HttpRequest::Method::Post);
}

DescribeDataCheckReportUrlRequest::~DescribeDataCheckReportUrlRequest() {}

std::string DescribeDataCheckReportUrlRequest::getJobStepId() const {
  return jobStepId_;
}

void DescribeDataCheckReportUrlRequest::setJobStepId(const std::string &jobStepId) {
  jobStepId_ = jobStepId;
  setParameter(std::string("JobStepId"), jobStepId);
}

std::string DescribeDataCheckReportUrlRequest::getTbName() const {
  return tbName_;
}

void DescribeDataCheckReportUrlRequest::setTbName(const std::string &tbName) {
  tbName_ = tbName;
  setParameter(std::string("TbName"), tbName);
}

std::string DescribeDataCheckReportUrlRequest::getDbName() const {
  return dbName_;
}

void DescribeDataCheckReportUrlRequest::setDbName(const std::string &dbName) {
  dbName_ = dbName;
  setParameter(std::string("DbName"), dbName);
}

